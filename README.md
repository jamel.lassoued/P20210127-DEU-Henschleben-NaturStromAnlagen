# P20210127-DEU-Henschleben-NaturStromAnlagen 


## Overview

| Name                  | Info                                  |
|-----------------------|---------------------------------------|
| ESS Typ               | mtu Energypack QLarge HD              |
| ESS Size (kVA/kW/kWh) | 1000 kVA / 1000 kW / 1068 kWh         |
| Battery Type          | Samsung MM3f                          |
| Battery Config        | exp. 264S 5P                          |
| String Config         | 22 Strings  (1 String/Rack)           |
| Tray/Module Config    | 11 Trays / 12 Module (2 Modules/Tray) |
| Blackstart            | Yes                                   |
| Grid Typ              | Microgrid                             |
| Expected FAT          | 29.11.2021                            |
| Expected SAT          | 08.10.2021                            |
| Project Engineer      | Paul Miodek                           |
  
## Devices

| Product-Typ | Manufacturer | Model/Typ | SW-Module Name | Interface | Num | INFO |
|-------------|--------------|-----------|----------------|-----------|-----|------|
| HVAC        | Mitsubishi   | Stulz     | ESS_CTRL       | MB TCP    | 2   |      |
| Meter       | Bachmann     |           | GM260          | SVI       | 1   |      |
| Meter       | Bachmann     |           | GSP274         | SVI       | 1   |      |


## Licences/Interfaces

| TYP    | Needed | INFO                 |
|--------|--------|----------------------|
| Modbus | Yes    | for external Control |
| WebMI  | Yes    | for Frontend         |
| Scope  | Yes    | for history data     |
| OPC-UA | No     |                      |
| Other  | No     |                      |

## Links

- [Link to project](https://qinous.sharepoint.com/sites/Qinous-DMS/07CustomerProjects/Docs/Forms/AllItems.aspx?newTargetListUrl=%2Fsites%2FQinous%2DDMS%2F07CustomerProjects%2FDocs&viewpath=%2Fsites%2FQinous%2DDMS%2F07CustomerProjects%2FDocs%2FForms%2FAllItems%2Easpx&id=%2Fsites%2FQinous%2DDMS%2F07CustomerProjects%2FDocs%2FP20210127%20GER%20Henschleben%20Naturstrom&viewid=8ea96255%2Dcb66%2D4b8d%2D89fc%2D1cac8658fde3)
- [Link to circuit diagram](https://qinous.sharepoint.com/sites/Qinous-DMS/07CustomerProjects/Docs/Forms/AllItems.aspx?newTargetListUrl=%2Fsites%2FQinous%2DDMS%2F07CustomerProjects%2FDocs&viewpath=%2Fsites%2FQinous%2DDMS%2F07CustomerProjects%2FDocs%2FForms%2FAllItems%2Easpx&id=%2Fsites%2FQinous%2DDMS%2F07CustomerProjects%2FDocs%2FP20210127%20GER%20Henschleben%20Naturstrom%2F040%20%2D%20Design%2F042%20%2D%20Electrical%20Diagrams&viewid=8ea96255%2Dcb66%2D4b8d%2D89fc%2D1cac8658fde3)
- [Link to network topology](https://qinous.sharepoint.com/sites/Qinous-DMS/07CustomerProjects/Docs/Forms/AllItems.aspx?newTargetListUrl=%2Fsites%2FQinous%2DDMS%2F07CustomerProjects%2FDocs&viewpath=%2Fsites%2FQinous%2DDMS%2F07CustomerProjects%2FDocs%2FForms%2FAllItems%2Easpx&id=%2Fsites%2FQinous%2DDMS%2F07CustomerProjects%2FDocs%2FP20210127%20GER%20Henschleben%20Naturstrom%2F040%20%2D%20Design%2F044%20%2D%20IT%2DStructure&viewid=8ea96255%2Dcb66%2D4b8d%2D89fc%2D1cac8658fde3)
## Features

*Add additional features here!*
  
## Setup Devices
  
| Module  | Station | Card-ID | Slots | INFO                    |
|---------|---------|---------|-------|-------------------------|
| MX220   | 1       | 2       | 2     | 1xCAN, 1xRS485, 1xRS232 |
| LM201   | 1       | 3       | 1     | Empty                   |
| GIO212  | 1       | 4       | 1     | DIO/AIO/PT              |
| PTAI216 | 1       | 5       | 1     | AI/PT                   |
| DIO264  | 1       | 7       | 2     | DIO                     |
| GM260   | 1       | 8       | 1     | Meter                   |
| GSP274  | 1       | 11      | 3     | Meter                   |
| PTAI216 | 1       | 12      | 1     | AI/PT                   |
| LM201   | 1       | 13      | 1     | Empty                   |
